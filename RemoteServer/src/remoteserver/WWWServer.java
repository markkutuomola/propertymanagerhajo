package remoteserver;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import controlserver.ControlServer;


public class WWWServer {

    private HttpServer server;
    private RMIClient rmiClient;

    public WWWServer(RMIClient rmiClient) {
        this.rmiClient = rmiClient;
        //TODO: Create http-server instance and run it
        try {
            server = HttpServer.create(new InetSocketAddress(8888), 0);
            server.setExecutor(Executors.newCachedThreadPool());
            server.createContext("/status2", new MyHandler());
            server.createContext("/getlights", new MyHandler2());
            server.createContext("/settemp", new MyHandler3());
            server.createContext("/setlight", new MyHandler4());

            server.start();

        } catch (IOException e){
            e.printStackTrace();
        }


    }

    //TODO: Create handlers for requests
    class MyHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            String temp = rmiClient.getTemp();
            System.out.println(temp);
            t.sendResponseHeaders(200,temp.length());
            OutputStream output= t.getResponseBody();
            output.write(temp.getBytes());
            t.close();

        }
    }
    class MyHandler2 implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            String light1 = rmiClient.getLights();
            System.out.println(light1);
            t.sendResponseHeaders(200,light1.length());
            OutputStream output= t.getResponseBody();
            output.write(light1.getBytes());
            t.close();
        }
    }
    class MyHandler3 implements HttpHandler{
        public void handle(HttpExchange t) throws IOException{
            InputStreamReader isr = new InputStreamReader(t.getRequestBody(),"utf-8");
            BufferedReader br = new BufferedReader(isr);

            int b;
            StringBuilder buf =new StringBuilder();
            while  ((b =br.read()) !=-1){
                buf.append((char) b);
            }
            br.close();
            isr.close();
            int index = buf.toString().indexOf("=");
            String input = buf.substring(index+1, buf.length());
            System.out.println(input);
            rmiClient.setTemp(input);

            t.sendResponseHeaders(200,input.length());
            OutputStream output= t.getResponseBody();
            output.write(input.getBytes());
            t.close();

        }
    }
    class MyHandler4 implements HttpHandler{
        public void handle(HttpExchange t) throws IOException{
            InputStreamReader isr = new InputStreamReader(t.getRequestBody(),"utf-8");
            BufferedReader br = new BufferedReader(isr);

            int b;
            StringBuilder buf =new StringBuilder();
            while  ((b =br.read()) !=-1){
                buf.append((char) b);
            }
            br.close();
            isr.close();
            int index = buf.toString().indexOf("=");
            String input = buf.substring(index+1, buf.length());
            int id = Integer.parseInt(input);
            rmiClient.rmiToggleLight(id);

            t.sendResponseHeaders(200,input.length());
            OutputStream output= t.getResponseBody();
            output.write(input.getBytes());
            t.close();

        }

    }

}
