package remoteserver;

import controlserver.ControlServer;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface remoteInterface extends Remote {
     //String executeTask(String id)  throws RemoteException;

//     public void rmiSendStatus(int id, int input, String temp) throws RemoteException;
     ControlServer.Mode rmiGetLightStatus(int ID) throws RemoteException;
     String rmiGetTempStatus() throws RemoteException;
     void rmiToggleLight(int id) throws RemoteException;
     void rmiSetTemp(String temp) throws RemoteException;
}