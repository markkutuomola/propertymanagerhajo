package remoteserver;

import controlserver.ControlServer;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class RMIClient {

    remoteInterface comp;

    public RMIClient() {
        //Security manager is needed. Remember policy file and VM parameter again.
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        //TODO: RMI client connection
        try {
        comp = (remoteInterface) Naming.lookup("//localhost/RMIServer");

        }

        catch (NotBoundException e) {
            System.out.println("Not Bound Exception");
            e.printStackTrace();
        }
        catch (MalformedURLException e) {
            System.out.println("Malformed URL");
            e.printStackTrace();
        }
        catch (RemoteException e) {
            System.out.println("Remote Exception");
            e.printStackTrace();
        }


    }
   //TODO: Create needed requests to control server
    public String getTemp()throws RemoteException {
        return comp.rmiGetTempStatus();
    }
    public String  getLights() throws RemoteException {
        String status="";
        for (int i = 1; i< 9; i++){
            status +="Valo "+ i +" "+ comp.rmiGetLightStatus(i).toString() +", ";
        } return status;

    }
    public void setTemp(String temp) throws RemoteException{
        comp.rmiSetTemp(temp);
    }
    public void rmiToggleLight(int id) throws RemoteException{
        comp.rmiToggleLight(id);
    }
}


