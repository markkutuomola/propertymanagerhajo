package lightswitch;

import java.io.IOException;
import java.io.InputStream;


public class LightObject extends Thread {
    private Lightswitch ls;
    private InputStream inS;

    public LightObject(Lightswitch ls, InputStream inS) {
        this.ls = ls;
        this.inS = inS;
    }

    @Override
    public void run() {
        try {
            while (true) {
                int input = inS.read();
                System.out.println(input); //Testausta varten
                ls.setLightStatus(input);
                System.out.println("Nappia painettu");

            }

        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}
