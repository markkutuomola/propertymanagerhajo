package lightswitch;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.*;



public class Lightswitch {
    private JPanel mainPanel;
    private JButton switchbutton;
    private JLabel statuslabel;
    private Mode lightstatus;
    private enum Mode {OFF, ON, NOTCONNECTED}
    private static int ID;
    private InputStream inS;
    private OutputStream ouS;
    private LightObject lo;



    public Lightswitch() {
        switchbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendChange(ID);
            }
        });

        connectSwitch(ID);
    }

    protected void connectSwitch(int ID) {
        //TODO: Create an socket connection connection to server
        final int PORT = 3331;
        final String IP = "127.0.0.1";
        try {
            Socket s = new Socket(IP, PORT);
            System.out.println("Yhdistetty");
            inS = s.getInputStream();
            ouS = s.getOutputStream();

            lo = new LightObject(this, inS);
            lo.start();
            System.out.println("Connection ready..");
            try {
                ouS.write(ID);
                ouS.flush();

            } catch (IOException e) {
                statuslabel.setText("Write failed");
            }

        }
        catch (IOException io) {
            System.out.println("IO exception");
        }
    }

    protected void sendChange(int ID) {
        //TODO: Send lightswitch action pressed to server
        try {
            ouS.write(ID);
            ouS.flush();

        } catch (IOException e) {
            statuslabel.setText("Write failed");
        }
    }


//Setter for the status of the light
// Modes are OFF, ON, NOTCONNECTED
    public void setLightStatus(int input) {
            if (input == 1) {
                lightstatus = Mode.ON;
                statuslabel.setText("Lights on");
                statuslabel.setBackground(Color.green);
            }
            else if (input == 0){
                lightstatus = Mode.OFF;
                statuslabel.setText("Lights off");
                statuslabel.setBackground(Color.red);
            }
            else if (input == 0){
                lightstatus = Mode.NOTCONNECTED;
                statuslabel.setText("Not connected");
                statuslabel.setBackground(Color.yellow);
            }
    }

    public static void main(String[] args) {
        //No need to edit main.
        //ID number is read from th first command line parameter
        if (args.length >0) {
            try {
                ID = Integer.parseInt(args[0]);
            } catch (Exception ex) {
                System.err.println("Error reading arguments");
                ID = 0;
            }
        }
        else {ID = 0; }

        JFrame frame = new JFrame("Lightswitch");
        frame.setContentPane(new Lightswitch().mainPanel);
        frame.setTitle("Lightswitch");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        System.out.println(ID);
    }
}
