package controlserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import remoteserver.*;

public class RMIServer extends UnicastRemoteObject implements remoteInterface {

    private int id;
    private int state;
    private String temp;
    private ControlServer cs;

    public RMIServer (int RMIport, ControlServer cs) throws RemoteException {
        super(RMIport);
        this.cs = cs;

        //Constructor
//You will need Security manager to make RMI work
        //Remember to add security.policy to your run time VM options
        //-Djava.security.policy=[YOUR PATH HERE]\server.policy
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

    }
// lähetetään valontila
    public ControlServer.Mode rmiGetLightStatus(int id) {
        System.out.println(cs.getLightstatus(id));
        return cs.getLightstatus(id);
    }

    //haetaan lämpötila
    public String rmiGetTempStatus (){
        return cs.getTemperature();
    }

    public void rmiToggleLight(int id){
        cs.toggleLightstatus(id);
    }
    public void rmiSetTemp(String temp){
        cs.setTemp(temp);
    }
}

