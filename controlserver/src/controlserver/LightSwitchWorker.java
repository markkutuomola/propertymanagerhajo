package controlserver;

import javax.swing.*;
import java.io.*;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.*;
import java.sql.ClientInfoStatus;

public class LightSwitchWorker implements Runnable {
    private Socket client;
    private ControlServer cs;
    private lightswitchServer ls;
    private int id;
    private InputStream iS;
    private OutputStream oS;

    public LightSwitchWorker (Socket s, lightswitchServer ls){
        client = s;
        this.ls = ls;
    }

    public void run(){

        try {
            iS = client.getInputStream();


            try {
                boolean eka =true;

                while (true) {
                    id = iS.read();
                    System.out.println(id); //Testausta varten
                    if (eka) {
                        ls.saveSocket(id, this);
                        eka = false;
                    }
                    else{

                        ls.toggleLightstatus(id);
                        System.out.println("Nappia painettu");

                    }
                }

            } catch (Exception e){
                e.printStackTrace();
                client.close();
            }
        } catch (IOException e){
            e.printStackTrace();
            System.out.println("Exception");
        }
    }
    public void sendLightStatus (int input) {
        try {
            oS = client.getOutputStream();
            oS.write(input);
            oS.flush();

        } catch (IOException e) {
            e.printStackTrace();

        }
    }


}
