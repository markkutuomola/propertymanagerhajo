package controlserver;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class lightswitchServer extends Thread {
    private ServerSocket ss;
    private ControlServer cs;

    public static List<List<LightSwitchWorker>> sockets= Collections.synchronizedList(new ArrayList<>());

    public lightswitchServer(String IP, int port, ControlServer cs) {
        //constructor, create server here and bind it to IP & port
        this.cs = cs;
        for (int i= 0; i <9 ; i++) {
            sockets.add(new ArrayList<LightSwitchWorker>());
        }
        try {
            ss = new ServerSocket();
            ss.bind(new InetSocketAddress(InetAddress.getByName(IP), port));

        } catch (IOException e) {
            e.printStackTrace();

        }
    }
        public void run() {
            //And here you should listen to the server socket
            try {

                while (true) {
                    Socket s = ss.accept();
                    Runnable worker = new LightSwitchWorker(s,this);
                    Thread t = new Thread(worker);
                    t.start();
                    System.out.println("Socket connection created");

                }

            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("IO Exception again");
            }
        }
        public void saveSocket(int i, LightSwitchWorker workerObject){
            sockets.get(i).add(workerObject);
            System.out.println("Testi");
        }
        public void toggleLightstatus(int id){
            cs.toggleLightstatus(id);
        }
        public void sendLightStatus (int id, int input){
            for (int i = 0; i < sockets.get(id).size(); i++){
                sockets.get(id).get(i).sendLightStatus(input);
            }
        }
    }

